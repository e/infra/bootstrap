#!/bin/bash

################################################################################
# closed repo during test-phase, will be public later - testing with my account
USER=thilo
BOOTSTRAPREPO=https://${USER}@gitlab.e.foundation/e/priv/infra/compose.git
apt-get update && apt install -y --asume-yes true git salt-minion
################################################################################


# Clone repo
echo "Cloning repo .."
git -C /mnt clone $BOOTSTRAPREPO repo-base


# Init salt-minion (masterless)
cp /mnt/repo-base/deployment/salt/init-config/masterless.conf /etc/salt/minion.d/

# Run repo init (might run a few minutes)
echo "System update and packages installation .."
salt-call state.apply docker-compose


# init repo
cd /mnt/repo-base && bash init-repo.sh
