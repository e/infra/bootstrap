#!/bin/bash

if [[ $# -lt 1 ]]
then
    echo "Usage $0 <repo-url> [branch name] [environment]"
    exit 1
fi
REPO=$1
BRANCH="$2"
ENVIRONMENT="$3"
if [[ "$BRANCH" != "" ]]
then
    BRANCH="--branch $BRANCH"
fi
################################################################################
apt-get update && apt install -y --asume-yes true git salt-minion
################################################################################


# Clone repo
echo "Cloning repo .."
git -C /mnt clone ${REPO} ${BRANCH} repo-base


# Init salt-minion (masterless)
cp /mnt/repo-base/deployment/salt/init-config/masterless.conf /etc/salt/minion.d/

# Run repo init (might run a few minutes)
echo "System update and packages installation .."
salt-call state.apply docker-compose


# init repo
bash /mnt/repo-base/scripts/init-repo.sh $ENVIRONMENT
